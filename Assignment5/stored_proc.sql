DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_details`(IN `txt` CHAR(255))
select DISTINCT pokemon.*
from pokemon,type
where pokemon.name = txt$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_nameNumberType`()
select pokemon.name,pokemon.nat as number,type.type from pokemon,type where pokemon.name = type.name$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_type`(IN `txt` CHAR(255))
select type.type from type
where type.name = txt$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_name`(IN `select_type` CHAR(255))
select pokemon.name,pokemon.nat as number,type.type from pokemon,type where type = select_type$$
DELIMITER ;
