
<?php
 session_start();
 ?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title></title>
  </head>
  <body>


<?php
   require_once 'sqlhelper.php';
   require_once './vendor/autoload.php';  //include the twig library.
   $loader = new Twig_Loader_Filesystem('./templates'); //set to load from the ./templates directory

    $twig = new Twig_Environment($loader);

    //Sql setup

   $conn = connectToMyDatabase();
   $result = $conn->query("call getPopularNames()");

   ?>
    <div class="container">

    <?php                                //MY HEADING SECTION STARTS

      $table = $result->fetch_all(MYSQLI_ASSOC);
      ?>

      <div class="heading">
        <?php
          $template = $twig->load('pageHeading.twig.html');
        echo $template->render(array("heading"=> "My Favourite Pokemon"));        //MY HEADING SECTION ENDS
         ?>

      </div>
      <div class="sidebar">
      <?php                             //MY  sidebar SECTION starts

        //setup twig

      $template_sideBar = $twig->load('sideBar.twig.html');

      echo $template_sideBar->render(array("popularNames"=>$table));
      ?>
    </div>
    <div class="cards">
  <?php
  $conn->close();
  $conn1 = connectToMyDatabase();
  if(isset($_GET['name']))

  {
  $name=($_GET["name"]);
  $_SESSION['name'] =  $name;
  $res1 = $conn1->query("call get_cards(\"$name\")");;
  $table2 = $res1->fetch_all(MYSQLI_ASSOC);
  $template_table = $twig->load('cards.twig.html');
  echo $template_table->render(array("cards"=>$table2));

  }
  else {
    echo "MY Favourite card";
  }
  $conn1->close();

?>
</div>
    <div class="pokeTable">
    <?php                                                         //MY TABLE SECTION ENDS
      $conn2 = connectToMyDatabase();
      $res2 = $conn2->query("call get_nameNumberType()");
      $table3 = $res2->fetch_all(MYSQLI_ASSOC);
      //setup twig

      $template_table = $twig->load('table.twig.html');
      //called render to replace values in template with ones specified in my array

      echo $template_table->render(array("table"=>$table3));

      $conn2->close(); //clean up connection


?>
</div>
</div>
</body>
</html>
