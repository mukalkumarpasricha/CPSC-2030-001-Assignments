DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_cards`(IN `txt` CHAR(255))
select pokemon.name,pokemon.nat as number 
from pokemon where txt = pokemon.name$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_nameNumberType`()
select pokemon.name,pokemon.nat as number,type.type from pokemon,type where pokemon.name = type.name$$
DELIMITER ;


DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_details`(IN `txt` CHAR(255))
select DISTINCT pokemon.*,type.type
from pokemon,type
where type.name = txt AND pokemon.name = txt$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `getPopularNames`()
SELECT pokemon.* from pokemon
where name = "heatran" OR name = "manaphy" OR name = "mesprit"
OR name = "azelf" OR name = "cresselia" OR  name = "garchomp" OR name = "rhyperior" OR name = "togekiss" OR name = "rampardos" OR name = "tangrowth" OR name = "hippowdon" OR name = "electivire"$$
DELIMITER ;