let federateArmy = Array();
let squadList =    Array();


let description1 = "Born in the Gallian city of Hafen, this promising young squad leader is keenly attuned to climate changes and weather fluctuations. Ever since graduating as valedictorian from the Royal Military Academy, his talent and determination have been an inspiration to his subordinates.";

let description2 = "Born in the Gallian city of Hafen, this brilliant inventor was assigned to Squad E after researching ragnite technology in the United States of Vinland. She appears to share some history with Claude, although the memories seem to be traumatic ones";

let description3 =   "Born in the Gallian city of Hafen, this foul-mouthed Darcsen worked his way up from the slums to become a capable soldier. Though foul-mouthed and reckless, his athleticism and combat prowess is top-notch... And according to him, he's invincible";

let description4 =  "Born in the Gallian city of Hafen, this cool and collected sharpshooter has earned the codename 'Deadeye Kai.' Along with her childhood friends, she joined a foreign military to take the fight to the Empire. She loves fresh-baked bread, almost to a fault.";

let description5 = "Born in the United Kingdom of Edinburgh, this excitable driver was Claude Wallace's partner in tank training, and was delighted to be assigned to Squad E. He is taken up photography as a hobby, and is constantly taking snapshots whenever on standby. ";

let description6 = "Born in the United States of Vinland, this driver loves armored personnel carriers with a passion. His skill behind the wheel is matched only by his way with a wrench. Though not much of a talker, he takes pride in carrying his teammates through combat.";

let description7 = "Born in the United Kingdom of Edinburgh, this stern driver was Minerva Victor's underclassman at the military academy. Upon being assigned to Squad F, he swore an oath of fealty to Lt. Victor, and takes great satisfaction in upholding her chivalric code.";

let description8 =  "Born in the United Kingdom of Edinburgh, this naval officer commands a state-of-the-art snow cruiser named the Centurion. For a ship's captain, his disposition is surprisingly mild-mannered. As such, he never loses his composure, even in the direst of straits.";

let description9 = "As the Centurion's crewmember responsible for navigation, this straight-laced navy man is considered by his peers to be the ship's de facto first mate. He holds Cpt. Morgen in very high regard. Enjoys gardening, and keeps a collection of potted plants in his quarters.";

let description10 = "As the Centurion's crewmember responsible for overseeing daily operations, this gentle and supportive EWI veteran even takes daily tasks like cooking and cleaning upon herself. She never forgets to wear a smile. Her age is undisclosed, even in her personnel files." ;

//CLASS FOR SPECIFICATIONS
class specifications{
  constructor(name,side,unit,rank,role,description,image){
    this.name=name;
    this.side=side;
    this.unit=unit;
    this.rank=rank;
    this.role=role;
    this.description=description;
    this.image=image;
  }
}

//CALLING FUNCTION addelements();
addElements();

function addElements(){
  federateArmy.push(new specifications ("Claude Wallace","Edinburgh Army","Ranger Corps, Squad E","First Lieutenant","Tank Commander",description1,"images/char1.png"));
  federateArmy.push(new specifications("Riley Miller","Edinburgh Army","Federate Joint Ops","Second Lieutenant","Artillery Advisor",description2,"images/char2.png"));
  federateArmy.push(new specifications("Raz","Edinburgh Army","Ranger Corps, Squad E","Sergeant","Fireteam Leader",description3,"images/char3.png"));
  federateArmy.push(new specifications("Kai Schulen","Edinburgh Army","Ranger Corps, Squad E","Sergeant Major","Fireteam Leader",description4,"images/char4.png"));
  federateArmy.push(new specifications("Miles Arbeck","Edinburgh Army","Ranger Corps, Squad E","Sergeant","Tank Operator",description5,"images/char5.png"));
  federateArmy.push(new specifications("Dan Bentley","Edinburgh Army","Ranger Corps, Squad E","Private First Class","APC Operator",description6,"images/char6.png"));
  federateArmy.push(new specifications("Ronald Albee","Edinburgh Army","Ranger Corps, Squad F","Second Lieutenant","Tank Operator",description7,"images/char7.png"));
  federateArmy.push(new specifications("Roland Morgen","Edinburgh Navy","Centurion, Cygnus Fleet","Ship's Captain","Cruiser Commander",description8,"images/char8.png"));
  federateArmy.push(new specifications("Brian Haddock","Edinburgh Navy","Centurion, Cygnus Fleet","Lieutenant","Chief Navigator",description9,"images/char9.png"));
  federateArmy.push(new specifications("Marie Bennett","Edinburgh Navy"," Centurion, Cygnus Fleet","Petty Officer","Chief of Operations",description10,"images/char10.png"));
}

//MY list of federateArmy names
function myArmyList(){
  for (var i = 0; i < federateArmy.length; i++) {
    document.getElementById("list" +i).innerHTML =federateArmy[i].name;

  }
}

// MY PROFILE FUNCTION
function profile_image(index, myArmy){
  document.getElementById("profile").innerHTML = "<b>Name</b> : " + myArmy[index].name + "<br>" +
  "<b>Side</b> : " + myArmy[index].side  + "<br>" +
  "<b>Unit</b> : " + myArmy[index].unit  + "<br>" +
  "<b>Rank</b> : " + myArmy[index].rank  + "<br>" +
  "<b>Role</b> : " + myArmy[index].role  + "<br>" +
  "<br>" +
  "<b>Desciption</b> : " + myArmy[index].description ;

  var image = document.getElementById("images");
  image.src=myArmy[index].image ;

}

function addTosquad(index){
  if(squadList.indexOf(federateArmy[index])<0){
    if (squadList.length<5) {
      squadList.push(federateArmy[index]);
    }
    for (var i = 0; i <squadList.length; i++) {
      document.getElementById("squad" +i).innerHTML =squadList[i].name;

    }
  }
  isSelected(index);

}
// FUNCTION TO REMOVE
function remove(index){
  document.getElementById("list" +federateArmy.indexOf(squadList[index])).innerHTML = federateArmy[federateArmy.indexOf(squadList[index])].name ;
  document.getElementById("squad" +(squadList.length-1)).innerHTML = "";
  squadList.splice(index, 1);


  for (var i = 0; i <squadList.length; i++) {
    document.getElementById("squad" +i).innerHTML = squadList[i].name;

  }




}
function isSelected(index){
  if(squadList.indexOf(federateArmy[index])>=0){
    document.getElementById("list" +index).innerHTML ="<u>"+"<b>" + federateArmy[index].name + "</b>" +"</u>";
  }

}
