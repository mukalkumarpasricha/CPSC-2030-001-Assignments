<!DOCTYPE html>
<html>
<head>
	<title>Assignment9 </title>
	<link href="style.less" type="text/css" rel="stylesheet">
	<script type="text/javascript" src="https://code.jquery.com/jquery-1.12.4.min.js"> </script>
</head>
<body>
	<h1>MY CHAT ROOM </h1>
	<div class="wrapper">
		<form method="post" id="form">
			<label>Name:</label><br>
			<input type="text" name="userName" id="myUsername" placeholder="Please Enter Your Name" required><br>
			<label>Your Message:</label><br>

			<textarea name="message" id="message" placeholder="Please Enter Your Message" required></textarea>
			<br>

			<input type="button" value="send" id="store" name="submission">
		</form>
	</div>
	<div class="wrap">

		<h3 class="item" id="my_all_messages">Fetch All Messages</h3>
		<p class="fetch_all"></p>
		<h3 class="item" id="last_hour_message">Last Hour Messages</h3>
		<p class="last_hour"></p>

	</div>
</body>
</html>

<!--MY JAVA SCRIPT PORTION-->

<script type="text/javascript">
$(document).on('click','#store',function(e) {
	var data = $("#form").serialize();
	var user = $('#myUsername').val();
	var myMessage = $('#message').val();
	if (user == '' || myMessage == '') {
		alert("Empty Field!!!!!!!!!!!!!!!!!!!!");
	} else {
		$.ajax({
			data: data,
			type: "post",
			url: "setup.php",	success: function(data){
				alert("successfully Inserted");
				$('#form')[0].reset();
			}
		});
	}
});

function create_chat_room(line){
	var final_message = "";

	final_message = "<b>ServerTime:</b> "+line.serverTime + "<br>" +
	"<b>Username:</b>" +line.user_name + "<br>" +
	"<b>Message:</b>" +line.message;

	final_message = "<div class=\"indexItem\" idx=\""+line.idx+"\">"+final_message+"</div><br>";
	return final_message;
}


$(document).on('click','#last_hour_message',function(e) { 	// Last hour messages
	$.ajax({
		type: "GET",
		url: "getData.php?mode=my_last_hour",
		dataType: "html",
		success:function(data){
			var myData = JSON.parse(data);
			let myResult = "";
			for(var i = 0; i < myData.length; i++){
				myResult += create_chat_room(myData[i]);
			}
			$(".last_hour").html(myResult);
		}

	});
});


//creating a division for my chat room

$(document).on('click','#my_all_messages',function(e) {
	$.ajax({
		type: "GET",
		url: "getData.php?mode=allmessages",
		dataType: "html",
		success:function(data){
			var myData = JSON.parse(data);
			let myResult = "";
			for(var i = 0; i < myData.length; i++){
				myResult += create_chat_room(myData[i]);
			}
			$(".fetch_all").html(myResult);
		}

	});
});

</script>
