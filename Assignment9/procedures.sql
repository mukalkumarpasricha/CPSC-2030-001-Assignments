DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `dataByDesc`()
select serverTime,user_name,message from my_chat_room order by id desc$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `fetchData`()
select serverTime,user_name,message from my_chat_room where serverTime >= DATE_SUB(NOW(),INTERVAL 1 HOUR) limit 10$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `insertMessage`(IN `txt1` VARCHAR(255), IN `txt2` VARCHAR(255))
insert into my_chat_room(user_name,message)values(txt1,txt2)$$
DELIMITER ;
