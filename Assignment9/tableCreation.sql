
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

Create Database assignment9;
CREATE TABLE `my_chat_room` (
  `Id` int(11) NOT NULL,
  `serverTime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `user_name` varchar(255) DEFAULT NULL,
  `message` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO 'my_chat_room' ('Id', 'serverTime', 'user_name', 'message') VALUES
(01, '2018-11-26 08:31:27', 'Alvin', 'hey'),
(02, '2018-11-26 08:32:37', 'Mukal', 'Namaste');

ALTER TABLE 'my_chat_room'
MODIFY 'Id' int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=03;
