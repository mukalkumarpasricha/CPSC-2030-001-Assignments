<?php
session_start();
include_once 'sqlhelper.php';                                         //<!--REFERENCE :-https://youtu.be/LC9GaXkdxF8 YOUTUBE VIDEO-->
$conn=connectToMyDatabase();
if(isset($_POST['submit']))
{
  $uid= mysqli_real_escape_string($conn,$_POST['uid']);
  $pwd= mysqli_real_escape_string($conn,$_POST['pwd']);
  if ((empty($uid) || empty($pwd))) {
    header("Location: ../index.php?login=empty");
    exit();
  }else {
    $sql="call get_users(\"$uid\")";
    $result=mysqli_query($conn,$sql);
    $resultCheck= mysqli_num_rows($result);
  }

  if ($resultCheck < 1) {                                              //VALIDATION FOR THE RESULT
    header("Location: ../index.php?login=error");
    exit();
  }else {
    if ($row = mysqli_fetch_assoc($result)) {
      // de-hasing the $pwd
      $hashed_pwd_check= password_verify($pwd,$row['user_pwd']);
      if ($hashed_pwd_check == false) {
        header("Location: ../index.php?login=error");
        exit();
      }elseif ($hashed_pwd_check == true) {
        // login
        $_SESSION['usr_id'] = $row['user_id'];
        $_SESSION['usr_first'] = $row['first_name'];
        $_SESSION['usr_last'] = $row['last_name'];
        $_SESSION['usr_email'] = $row['email_address'];
        $_SESSION['usr_uid'] = $row['user_uid'];
        header("Location: ../index.php?login=success");
        exit();
      }
    }
  }

}
else {
  header("Location: ../index.php?login=error");
  exit();
}

?>
