<?php
session_start();
if(isset($_POST['submit'])){
  include_once 'sqlhelper.php';                                                                  //REFERENCE :-https://youtu.be/LC9GaXkdxF8 YOUTUBE VIDEO
  $conn=connectToMyDatabase();
  $first = mysqli_real_escape_string($conn,$_POST['first']);
  $last = mysqli_real_escape_string($conn,$_POST['last']);
  $email = mysqli_real_escape_string($conn,$_POST['email']);
  $uid = mysqli_real_escape_string($conn,$_POST['uid']);
  $pwd = mysqli_real_escape_string($conn,$_POST['pwd']);
  // error handlers
  //check for empty columns
  if ((empty($first) || empty($last)) || empty($email) || empty($uid) || empty($pwd)) {
    header("Location: ../signup.php?signup=empty");
    exit();
  }
  else {
    //check if input is valid
    if (!preg_match("/^[a-zA-Z]*$/",$first) || !preg_match("/^[a-zA-Z]*$/",$last )) {
      header("Location: ../signup.php?signup=invalid");
      exit();
    }else {
      //check for valid $email
      if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        header("Location: ../signup.php?signup=invalidEmail");
        exit();
      }else {
        $sql="SELECT * FROM  users WHERE user_uid ='$uid'";
        $result=mysqli_query($conn,$sql);
        $resultCheck= mysqli_num_rows($result);
        if ($resultCheck > 0) {
          header("Location: ../signup.php?signup=user");
          exit();
        }else {
          //hasing the Password
          $hashed_pwd =password_hash($pwd, PASSWORD_DEFAULT);
          //insert user data to table
          $sql="CALL insertQuery(\"$first\",\"$last\",\"$email\",\"$uid\",\"$hashed_pwd\")";
          mysqli_query($conn,$sql);
          header("Location: ../signup.php?signup=success");
          exit();
        }
      }
    }
  }
}
else {
  header("Location: ../signup.php");
  exit();
}
?>
