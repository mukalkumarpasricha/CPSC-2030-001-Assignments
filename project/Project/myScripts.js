$(document).ready(function(){
  $("#input").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $(".cards").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});
//REFERENCE:- W3 schools

$(document).on('click','#store',function(e) {
  var data = $("#form").serialize();
  var user = $('#myUsername').val();
  var myMessage = $('#message').val();
  if (user == '' || myMessage == '') {
    alert("Empty Field!!!!!!!!!!!!!!!!!!!!");
  } else {
    $.ajax({
      data: data,
      type: "post",
      url: "setup.php",	success: function(data){
        alert("THANK YOU FOR YOUR PRECIOUS REVIEW");
        $('#form')[0].reset();
      }
    });
  }
});

$(document).on('click','#buystore',function(e) {
  var data = $("#form").serialize();
  var user = $('#myUsername').val();
  var Address = $('#Address').val();
  var Product = $('#Product').val();
  if (user == '' || Address == '') {
    alert("Empty Field!!!!!!!!!!!!!!!!!!!!");
  } else {
    $.ajax({
      data: data,
      type: "post",
      url: "buy_setup.php",	success: function(data){
        alert("Thank you for your Purchase, Delivery will take 5-7 working days");
        $('#form')[0].reset();
      }
    });
  }
});

function feedback(line){
  var final_message = "";

  final_message = "<b>Time:</b> "+ line.serverTime + "<br>" +
  "<b>Name:</b> " +line.user_name + "<br>" +
  "<b>Review:</b> " +line.message;

  final_message = "<div class=\"indexItem\" idx=\""+line.idx+"\">"+final_message+"</div><br>";
  return final_message;
}



//creating a division for my chat room

$(document).on('click','#my_all_messages',function(e) {
  $.ajax({
    type: "GET",
    url: "getData.php?mode=allmessages",
    dataType: "html",
    success:function(data){
      var myData = JSON.parse(data);
      let myResult = "";
      for(var i = 0; i < myData.length; i++){
        myResult += feedback(myData[i]);
      }
      $(".fetch_all").html(myResult);
    }

  });
});
