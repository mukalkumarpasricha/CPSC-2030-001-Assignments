create database project;

CREATE table users (
   user_id int(11) not null AUTO_INCREMENT PRIMARY key,
   first_name varchar(256) not null,
   last_name varchar(256) not null,
   email_address varchar(256) not null,
   user_uid varchar(256) not null,
   user_pwd varchar(256) not null
);



CREATE table products (
   product_pic varchar(255) not null,
   product_brand varchar(255) not null,
   product_name varchar(255) not null
);


CREATE table details (
   product_name varchar(255) not null,
   product_price varchar(255) not null,
   product_memory varchar(255) not null,
   product_camera varchar(255) not null,
   product_battery varchar(255) not null,
   product_display varchar(255) not null
);


insert into products(product_pic,product_brand,product_name )
values('iphone6.jpg','APPLE','Iphone 6'),
('iphone6s.jpg','APPLE','Iphone 6s'),
('iphone7.jpg','APPLE','Iphone 7'),
('iphone8.jpg','APPLE','Iphone 8'),
('iphonex.jpg','APPLE','Iphone x'),
('iphonexs.jpg','APPLE','Iphone xs'),
('sGalaxyNote9.jpg','SAMSUNG','Galaxy Note 9'),
('note8.jpg','SAMSUNG','Galaxy Note 8'),
('s8.jpg','SAMSUNG','Samsung S8'),
('s8+.jpg','SAMSUNG','Samsung S8+'),
('a8.jpg','SAMSUNG','Samsung Galaxy A8'),
('a6+.jpg','SAMSUNG','Samsung Galaxy A6+'),
('s7.jpg','SAMSUNG','Samsung S7'),
('a5.jpg','SAMSUNG','Samsung A5'),
('z2.jpg','SONY','Sony Xperia Z2'),
('z1.jpg','SONY','Sony Xperia Z1'),
('zr.jpg','SONY','Sony Xperia Zr'),
('z.jpg','SONY','Sony Xperia Z'),
('t2.jpg','SONY','Sony T2 Ultra Dual'),
('sp.jpg','SONY','Sony Xperia SP'),
('m.jpg','SONY','Sony Xperia M dual'),
('g6.jpg','LG','LG G6'),
('v20.jpg','LG','LG V20'),
('g5.jpg','LG','LG G5'),
('g4.jpg','LG','LG G4'),
('lgx.jpg','LG','LG X SCREEN'),
('gp3.jpg','GOOGLE','GOOGLE PIXEL3 XL'),
('2xl.jpg','GOOGLE','Google Pixel 2XL'),
('pixel2.jpg','GOOGLE','Google Pixel 2'),
('pixel.jpg','GOOGLE','Google Pixel')



;
insert into details(product_name,product_price,product_memory,product_camera,product_battery,product_display )
values('Iphone 6','$246.95','64 GB','8 MP',1715,'4.7 inches'),
('Iphone 6s','$344.99','128 GB','12 MP',1810,'4.7 inches'),
('Iphone 7','$500.00','32 GB','12 MP',1960,'4.7 inches'),
('Iphone 8','$999.00','128 GB','12 MP',1821,'4.7 inches'),
('Iphone x','$1265.00','64 GB','12 MP',2716,'5.5 inches'),
('Iphone xs','$1500.37','256 GB',' MP',2658,'5.8 inches'),
('Galaxy Note 9','$1300.00','128 GB','12 MP',4000,'6.4 inches'),
('Galaxy Note 8','$800.00','64 GB','12 MP',3300,'6.3 inches'),
('Samsung S8','$619.99','64 GB','12 MP',3500,'5.8 inches'),
('Samsung S8+','$675.00','128 GB','12 MP',3500,'6.2 inches'),
('Samsung Galaxy A8','$459.00','32 GB','16 MP',3000,'5.6 inches'),
('Samsung Galaxy A6+','$479.99','256 GB','16 MP',3500,'6 inches'),
('Samsung S7','$439.99','64 GB','12 MP',3000,'5.1 inches'),
('Samsung A5','$429.99','32 GB','13 MP',2300,'5.2 inches'),
('Sony Xperia Z2','$354.99','32 GB','20 MP',3200,'5.2 inches'),
('Sony Xperia Z1','$329.99','16 GB','20 MP',2300,'4.3 inches'),
('Sony Xperia Zr','$400','32 GB','13 MP',2300,'4.5 inches'),
('Sony Xperia Z','$256.56','16 GB','13 MP',2300,'5 inches'),
('Sony T2 Ultra Dual','$426.56','8 GB','13 MP',3000,'6 inches'),
('Sony Xperia SP','$420.99','8 GB','8 MP',2370,'4.6 inches'),
('Sony Xperia M dual','$320.99','4 GB','5 MP',1750,'4 inches'),
('LG G6','$520.99','32 GB','13 MP',1750,'5.7 inches'),
('LG V20','$620.99','32 GB','16 MP',2000,'5.7 inches'),
('LG G5','$421.99','32 GB','16 MP',2800,'5.3 inches'),
('LG G4','$400.00','32 GB','16 MP',3000,'5.5 inches'),
('LG X SCREEN','$299.00','16 GB','13 MP',2300,'4.93 inches'),
('GOOGLE PIXEL3 XL','$1099.00','64 GB','12 MP',3430,'6.3 inches'),
('Google Pixel 2XL','$609.00','64 GB','12.2 MP',3520,'6 inches'),
('Google Pixel 2','$900.00','64 GB','12 MP',2700,'5 inches'),
('Google Pixel','$800.00','32 GB','12.3 MP',2770,'5 inches')
;

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";
CREATE TABLE `feedback` (
  `Id` int(11) NOT NULL Primary key AUTO_INCREMENT,
  `serverTime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `user_name` varchar(255) DEFAULT NULL,
  `message` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `buy_list` (
  `Id` int(11) NOT NULL Primary key AUTO_INCREMENT,
  `Address` varchar(255) DEFAULT NULL,
  `user_name` varchar(255) DEFAULT NULL,
  `Product` varchar(255) DEFAULT NULL
);


INSERT INTO `feedback` (Id, serverTime, user_name, message) VALUES
(01, "2018-11-26 02:41:15", "Alvin", "AWESOME"),
(02, "2018-11-26 02:45:31", "Mukal", "GREAT");

INSERT INTO buy_list(Id,Address,user_name,Product) VALUES
(01, "7391 knight street", "Alvin", "Sony"),
(02, "King Edward street", "Mukal", "Apple");
