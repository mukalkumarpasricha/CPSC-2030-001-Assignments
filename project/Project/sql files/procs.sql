DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_users`(IN `txt` VARCHAR(256))
SELECT * FROM  users WHERE user_uid = txt$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `insertQuery`(IN `first` VARCHAR(256), IN `last` VARCHAR(256), IN `email` VARCHAR(256), IN `uid` VARCHAR(256), IN `pwd` VARCHAR(256))
INSERT INTO users (first_name,last_name,email_address,user_uid,user_pwd) VALUES(first,last,email,uid,pwd)$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `dataByDesc`()
select serverTime,user_name,message from feedback order by id desc$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `fetchData`()
select serverTime,user_name,message from feedback where serverTime >= DATE_SUB(NOW(),INTERVAL 1 HOUR) limit 10$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `insertMessage`(IN `txt1` VARCHAR(255), IN `txt2` VARCHAR(255))
insert into feedback(user_name,message)values(txt1,txt2)$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_all_products`()
select * from products$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_image`(IN `txt` VARCHAR(255))
select product_pic from products
where product_name=txt$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_details`(IN `txt` VARCHAR(255))
select * from details
where product_name=txt$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_brand`(IN `txt` VARCHAR(255))
select * from products
where product_brand=txt$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `buy`(IN `txt1` VARCHAR(255), IN `txt2` VARCHAR(255), IN `txt3` VARCHAR(255))
insert into buy_list(Address,user_name,Product)values(txt1,txt2,txt3)$$
DELIMITER ;
