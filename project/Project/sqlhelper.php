<?php

function clearConnection($mysql){
  while($mysql->more_results()){
    $mysql->next_result();
    $mysql->use_result();
  }
}
//helper function
function wrap($tag,$value) { //expect strings for both parameters
  return "<$tag>$value</$tag>";
}

//DB setup for this web-app
function connectToMyDatabase(){
  $user = 'PROJECT';
  $pwd = 'PROJECT';
  $server = 'localhost';
  $dbname = 'project';

  $conn = new mysqli($server, $user, $pwd, $dbname);
  return $conn;
}
//twig setup for this web-app
function setupMyTwigEnvironment(){
  $loader = new Twig_Loader_Filesystem('./templates'); //set to load from the ./templates directory
  $twig = new Twig_Environment($loader);
  return $twig;
}

function dumpErrorPage($twig){
  $template = $twig->load("error.twig.html");
  echo $template->render(array("message"=>"SQL errorm query failed"));
}

?>
