
<!DOCTYPE html>

<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet/less" type="text/css" media="screen" href="project.less" />
  <script src="https://cdnjs.cloudflare.com/ajax/libs/less.js/3.7.1/less.min.js"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="myScripts.js"></script> <!--REFERENCE:- W3 schools-->
  <title>MY DREAM PROJECT</title>
</head>
<body>
  <?php
  session_start();
  require_once './vendor/autoload.php';  //include the twig library.
  $loader = new Twig_Loader_Filesystem('./templates'); //set to load from the ./templates directory

  $twig = new Twig_Environment($loader);

  ?>
  <main>
    <header>
      <?php
      if(isset($_SESSION['usr_id']))
      {
        echo  '<form class="" action="includes/logout.php" method="post">
        <button type="submit" name="submit">Logout</button>
        </form>'.'<h3> Welcome'.' '.$_SESSION['usr_first'].'</h3>';
      }else {
        echo '<form action="includes/login.php" method ="POST">
        <input type="text" name="uid" placeholder="username/email">
        <input type="password" name="pwd" placeholder="password">
        <button type="submit" name="submit">Login</button>

        </form>'.'  <a class="signup_button" href="signup.php">Sign up</a>';
      }
      ?>



    </header>
    <?php
    $template = $twig->load('pageHeading.twig.html');
    echo $template->render(array("heading"=> " Hello Future"));        //MY HEADING SECTION ENDS
    ?>

    <nav id="box">
      <?php
      $template = $twig->load('menuBar.twig.html');
      echo $template->render(array("pageName1"=> "index.php","pageName2"=> "page2.php","pageName3"=> "page3.php","pageName4"=> "page4.php"
      ,"pageName6"=> "page6.php","pageName7"=> "page7.php","pageName8"=> "page8.php",
      "link1"=>"Home","link2"=>"Site Map","link3"=>"Products","link4"=>"Brands","link5"=>"Services","link6"=>"Feedback(reviews)"
      ,"link7"=>"Help & Contact","link8" => "About Us"));        //MY HEADING SECTION ENDS
      ?>
    </nav>
